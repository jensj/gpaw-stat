from ase.build import mx2
from gpaw import GPAW, PW, FermiDirac


def mos2() -> None:
    """MoS2 slab with 1200 bands (needed for a GW calculation)."""
    slab = mx2(formula='MoS2', kind='2H', a=3.184, thickness=3.127,
               size=(2, 2, 1), vacuum=4.5)
    slab.pbc = (1, 1, 1)
    slab.calc = GPAW(mode=PW(500),
                     xc='PBE',
                     basis='dzp',
                     kpts={'size': (3, 3, 1), 'gamma': True},
                     occupations=FermiDirac(0.01),
                     txt='mos2.txt')
    slab.get_potential_energy()
    slab.calc.fixed_density(nbands=1200,
                            convergence={'bands': 1100},
                            txt='mos2-1200.txt').write('mos2.gpw', 'all')
