import subprocess
from datetime import date
from pathlib import Path
import os


def run(cmd: str) -> subprocess.CompletedProcess:
    """subprocess.run() helper."""
    print(cmd)
    return subprocess.run(cmd, shell=True, check=True)


def main() -> None:
    """Submit all benchmarks to Niflheim."""
    home = Path.home() / 'GPAW-BENCHMARKS'
    day = str(date.today())
    root = home / day
    root.mkdir()
    os.chdir(root)
    run('../gpaw_venv.py venv')
    run('git clone -q git@gitlab.com:jensj/gpaw-benchmarks')

    bm4g = root / 'gpaw-benchmarks'
    activate = root / 'venv/bin/activate'

    if 0:
        run(f'. {activate} && pip install -e gpaw-benchmarks')
    else:
        activate.write_text(activate.read_text() +
                            f'export PYTHONPATH={bm4g}:$PYTHONPATH\n')

    run(f'. {activate} && mq init')

    wf = bm4g / 'bm4g/workflow.py'
    run(f'. {activate} && mq workflow {wf}')


if __name__ == '__main__':
    main()
