import json
import sys
from pathlib import Path

from bm4g.data import data

DATA = Path(__file__).with_name('data.py')


def add(path: Path) -> None:
    """Update results from benchmark."""
    dct = json.loads(path.read_text())
    name = path.parent.name
    day = path.parent.parent.name
    data.setdefault(name, []).append((day, dct['memory'], dct['time']))


def main() -> None:
    """Update data.py with new results."""
    for path in sys.argv[1:]:
        print(path)
        add(Path(path).absolute())

    text = 'from __future__ import annotations\n\n'
    text += 'data: dict[str, list[tuple[str, int, float]]] = {\n'
    for name in sorted(data):
        text += (f'    {name!r}:\n    [' +
                 ',\n     '.join(f'({day!r}, {mem:_}, {time:.3f})'
                                 for day, mem, time in sorted(data[name])) +
                 '],\n')
    text = text[:-2] + '}\n'
    DATA.write_text(text)


if __name__ == '__main__':
    main()
