import ase.units as units
from ase import Atoms
from ase.build import graphene
from ase.io import Trajectory
from ase.md.verlet import VelocityVerlet
from gpaw import GPAW, PW


def md() -> None:
    """MD for H atom flying through graphene sheet."""
    a = 2.45
    gra = graphene(a=a, size=(5, 5, 1), vacuum=10)
    gra.center()

    # Starting position of the projectile with an impact point at the
    # center of a hexagon.
    # Set mass to one atomic mass unit to avoid isotope average.
    atoms = gra + Atoms('H')
    d = a / 3**0.5
    atoms.positions[-1] = atoms.positions[22] + (0, d, 5)
    atoms.pbc = (True, True, True)

    calc = GPAW(mode=PW(500),
                nbands=110,
                xc='LDA',
                txt='md.txt')

    atoms.calc = calc
    atoms.get_potential_energy()

    # Moving to the MD part
    ekin = 100  # kinetic energy of the ion (in eV)
    timestep = 0.1  # timestep in fs

    # Integrator for the equations of motion, timestep depends on system
    dyn = VelocityVerlet(atoms, timestep * units.fs)

    # Saving the positions of all atoms after every time step
    with Trajectory('c100h.traj', 'w', atoms) as traj:
        dyn.attach(traj.write, interval=1)

        # Running one timestep before impact
        dyn.run(1)

        # Giving the target atom a kinetic energy of ene in the -z direction
        atoms[-1].momentum[2] = -(2 * ekin * atoms[-1].mass)**0.5

        # Running the simulation for 80 timesteps
        dyn.run(80)
