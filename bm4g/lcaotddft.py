from gpaw.lcaotddft import LCAOTDDFT
from gpaw.lcaotddft.dipolemomentwriter import DipoleMomentWriter


def lcaotddft() -> None:
    """LCAO-TDDFT time propagation."""
    parallel = {'sl_auto': True, 'domain': 2, 'augment_grids': True}
    td_calc = LCAOTDDFT('../ag55/ag55.gpw', parallel=parallel, txt='td.out')
    DipoleMomentWriter(td_calc, 'dm.dat')
    td_calc.absorption_kick([1e-5, 0.0, 0.0])
    td_calc.propagate(10, 3000)
