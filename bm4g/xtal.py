from ase.build import bulk
from gpaw import GPAW, PW
from ase.constraints import ExpCellFilter
from ase.optimize import BFGS
from typing import Any


def xtal(mode: str, xc: str) -> None:
    """HCP 16 atom Ru x-tal (only 2 atoms for HSE06).

    For PW-mode and PBE, the cell is optimized.  For all other
    cases only a single point calculation is performed.
    """
    atoms = bulk('Ru', a=2.6, covera=1.6)
    if xc == 'PBE':
        atoms = atoms.repeat((2, 2, 2))
        density = 4.5
    else:
        density = 2.0
    params: dict[str, Any]
    if mode == 'pw':
        params = dict(mode=PW(600))
    else:
        params = dict(mode=mode, h=0.17, basis='dzp')
    atoms.calc = GPAW(**params,
                      kpts={'density': density},
                      xc=xc,
                      txt='ru.txt')
    if xc == 'PBE' and mode == 'pw':
        BFGS(ExpCellFilter(atoms),
             trajectory='ru.traj',
             logfile='ru.log').run(fmax=0.02)
    else:
        atoms.get_forces()
