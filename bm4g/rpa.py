from ase import Atoms
from ase.build import fcc111
from gpaw import GPAW, PW, Davidson, FermiDirac, MixerSum
from gpaw.hybrids.energy import non_self_consistent_energy as nsc_energy
from gpaw.xc.rpa import RPACorrelation


def c2cu() -> None:
    """RPA-energy calculation for graphene on Cu(111)."""
    # Lattice parameter of Cu:
    a = 2**0.5 * 2.56
    d = 3.25
    slab = fcc111('Cu', a=a, size=(1, 1, 4), vacuum=10.0)
    slab.pbc = True
    slab += Atoms('C2',
                  scaled_positions=[[0, 0, 0],
                                    [1 / 3, 1 / 3, 0]],
                  cell=slab.cell)
    slab.positions[4:6, 2] = slab.positions[3, 2] + d

    slab.calc = GPAW(xc='PBE',
                     mode=PW(800),
                     basis='dzp',
                     eigensolver=Davidson(niter=4),
                     nbands='200%',
                     kpts={'size': (12, 12, 1), 'gamma': True},
                     occupations=FermiDirac(width=0.05),
                     convergence={'density': 1e-5},
                     parallel={'domain': 1},
                     mixer=MixerSum(0.05, 5, 50),
                     txt='pbe.txt')
    slab.get_potential_energy()

    e_hf = nsc_energy(slab.calc, 'EXX').sum()

    slab.calc.diagonalize_full_hamiltonian()
    slab.calc.write('pbe.gpw', mode='all')

    rpa = RPACorrelation('pbe.gpw',
                         txt='RPAc.txt',
                         skip_gamma=True,
                         frequency_scale=2.5,
                         ecut=200)
    e_rpac = rpa.calculate()[0]
    print(e_hf + e_rpac)
