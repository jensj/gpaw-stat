from gpaw.response.g0w0 import G0W0


def gw() -> None:
    """G0W0 calculation for MoS2 slab."""
    G0W0(calc='../mos2/mos2.gpw',
         bands=(8 * 4, 18 * 4),
         ecut=150,
         nbands=1100,
         truncation='2D',
         nblocksmax=True,
         q0_correction=True,
         filename='g0w0').calculate()
