import datetime
from collections import defaultdict
from pathlib import Path

import matplotlib.pyplot as plt  # type: ignore
import numpy as np

from bm4g.data import data

events = {
    '2022-01-12': 'GPAW-22.1.0',
    '2022-08-18': 'GPAW-22.8.0'}


def p() -> None:
    """Create csv and png files."""
    tables = defaultdict(list)
    mfig, _ = plt.subplots()
    tfig, _ = plt.subplots()
    for name, results in data.items():
        mem = np.array([m for _, m, _ in results])
        time = np.array([t for _, _, t in results])
        days = [datetime.date.fromisoformat(d)
                for d, *_ in results]
        for kind, values, fig in [('memory', mem, mfig),
                                  ('time', time, tfig)]:
            index = values.argmin()
            best = values[index]
            if kind == 'memory':
                desc = f'{best * 1e-9:.3f} GB'
            else:
                h, s = divmod(best, 60)
                desc = f'{h:.0f}h{s:02.0f}s'
            day = days[index]
            worst = values.max() / best
            last = values[-1] / best
            row = (f'{name}, '
                   f'{desc} ({day}), '
                   f'{worst * 100:.1f}%, '
                   f'{last * 100:.1f}%')
            tables[kind].append(row)
            # label = f'{name} ({best})'
            fig.axes[0].plot(days, values / best)

    for kind, rows in tables.items():
        Path(f'{kind}.csv').write_text(
            'benchmark, best, worst, last\n' +
            '\n'.join(rows) +
            '\n')

    for kind, fig in [('memory', mfig),
                      ('time', tfig)]:
        # fig.axes[0].legend()
        fig.savefig(f'{kind}.png')


if __name__ == '__main__':
    p()
