from __future__ import annotations

from typing import Any

from ase.build import fcc111
from ase.constraints import FixAtoms
from ase.optimize import GPMin
from gpaw import GPAW, PW


def slab(mode: str) -> None:
    """Pt FCC 3x3 (111) slab with 4 layers and a hydrogen atom on top."""
    atoms = fcc111('Pt', a=4., size=(3, 3, 4), vacuum=5.0)
    z = atoms.positions[:, 2].max()
    atoms.append('H')
    atoms.positions[-1, 2] = z + 1.9
    mask = atoms.get_tags() >= 3
    atoms.constraints = FixAtoms(mask=mask)
    params: dict[str, Any]
    if mode == 'pw':
        params = dict(mode=PW(600))
    else:
        params = dict(mode=mode, h=0.18, basis='dzp')
    atoms.calc = GPAW(**params,
                      xc='PBE',
                      kpts=(3, 3, 1),
                      txt='pt.txt')
    GPMin(atoms,
          trajectory='pt.traj',
          logfile='pt.log').run(fmax=0.05)
