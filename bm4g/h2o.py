from __future__ import annotations

from typing import Any

from ase import Atoms
from ase.optimize import GPMin
from gpaw import GPAW, PW


def h2o(mode: str) -> None:
    """Water molecule."""
    atoms = Atoms('H2O',
                  [(0, 0, 0),
                   (2, 1, 0),
                   (1, 0, 0)])
    atoms.center(vacuum=7.0)
    params: dict[str, Any]
    if mode == 'pw':
        params = dict(mode=PW(500))
    else:
        params = dict(mode=mode, h=0.16, basis='dzp')
    atoms.calc = GPAW(**params,
                      xc='PBE',
                      txt='h2o.txt')
    GPMin(atoms,
          trajectory='h2o.traj',
          logfile='h2o.log').run(fmax=0.01)
