from __future__ import annotations

import resource
import time
from functools import wraps
from pathlib import Path
from typing import Any, Callable

from myqueue.workflow import RunHandle
from myqueue.workflow import run as _run
from myqueue.workflow import runner
from typing_extensions import ParamSpec

from bm4g.ag55 import ag55
from bm4g.gw import gw
from bm4g.md import md
from bm4g.h2o import h2o
from bm4g.lcaotddft import lcaotddft
from bm4g.mos2 import mos2
from bm4g.rpa import c2cu
from bm4g.slab import slab
from bm4g.xtal import xtal

P = ParamSpec('P')


def measure(func: Callable[P, None]) -> Callable[P, dict[str, float]]:
    """Decorator for measuring time and memory use."""
    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> dict[str, float]:
        """Measure time and memory use."""
        t1 = time.time()
        func(*args, **kwargs)
        t2 = time.time()
        memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss * 1024
        return {'time': t2 - t1, 'memory': memory}
    return wrapper


def run(function: Callable,
        name: str,
        *,
        cores: int,
        **kwargs: Any) -> RunHandle:
    """Run banchmark in folder (and create the folder also)."""
    folder = Path(name)
    if not runner.target:
        folder.mkdir(exist_ok=True)
        if cores == 40:
            kwargs['nodename'] = 'xeon40_clx'
    return _run(function=measure(function),
                folder=folder,
                name=name,
                cores=cores,
                **kwargs)


def workflow() -> None:
    """Workflow for all the benchmarks."""
    for n in [24, 40, 56]:
        for mode in ['lcao', 'pw', 'fd']:
            run(slab, f'slab-{mode}-{n}', args=[mode], cores=n, tmax='1d')

    for mode, xc in [('pw', 'PBE'),
                     ('pw', 'HSE06'),
                     ('lcao', 'PBE'),
                     ('fd', 'PBE')]:
        name = f'xtal-{mode}-{xc}'
        if mode == 'pw' and xc == 'PBE':
            name += '-relax'
        run(xtal, name, args=[mode, xc], cores=n, tmax='10h')

    with run(mos2, 'mos2', cores=24, tmax='20h'):
        run(gw, 'gw', cores=56, tmax='1d')

    with run(ag55, 'ag55', cores=24, tmax='20h'):
        run(lcaotddft, 'lcaotddft', cores=56, tmax='1d')

    for mode in ['lcao', 'pw', 'fd']:
        run(h2o, f'h2o-{mode}', args=[mode], cores=24, tmax='1d')

    run(c2cu, 'c2cu', cores=40, tmax='1d')

    run(md, 'md', cores=40, tmax='1d')
